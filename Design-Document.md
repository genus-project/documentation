# Overview

## Objective
There is a disparity in terms of the information that students have available and the experience of course selection and professor vetting. Resources like RateMyProfessor or course evaluations are not automatically factored into course selection. This project aims to make the course selection process easier.

This document is intended for people to understand what the motiviations were behind this projects, and how it will be done.

## Background

There are two key information resources that help students decide on courses and professors; historic course evaluations and professor ratings from RateMyProfessor (RMP). There is also course information that is not currently being used optimally in the selection process, such as additional filters.

I have previously created a chrome extension with RMP data that acquired 1500+ users. There's a definetly a need for this type service.

## Alternatives
There are two main alternatives.

[Cobalt-Uoft](https://github.com/cobalt-uoft) - An open unofficial API providing a vast array of aggregated data.

This API does not meet the complete needs of this project as it does not provide course evaluation and RMP data. The API also has a rate limit which may hinder the scalability of this application.

[UofT-Prof-Analyzer](https://github.com/Walden-Shen/uoft-profs/) - A professor analyzer using course evaluation data.

This project has inspired [Genus-Project](https://gitlab.com/genus-project/).

This project does not have an open API for course evaluation data nor does it provide RMP ratings. The Genus Project wants to build ontop of this idea of analyzing professors and add course analysis leveraging the larger data pool.

The Genus Project has the potential for course scheduling and timetable permutation due too its data pool, so I would like to refrain from using an existing project if that opportunity comes to fruition.

## High-Level Design
The main goal of this project is to isolate all core functionality into its own repository and individual build/deployment pipelines.

The key reasons behind this are;
* Modularity - ability to change technologies for different components, loose coupling.
* Ease of development - simpler to switch frameworks.
* Scalability - Compute heavy resources such as APIs don't compete for CPU/Memory usage with APIs.


This system will have three main sections:

Front End Interface -  This is what the user interacts with. This may be completed as a web application or a messenger bot.

Web Scrapers - These are automated scripts that aggregate data from various sources and update gitlab repositiories with data in a way that is easy to ingest.

Back End - This is the core application logic and calculations. This service ingests latest data and provides information about the data in a way that is easy to build around.

![Image of Workflow](Design_Document_Workflow.png)

## Code Location
All code is currently under the project header [Genus-Project](https://gitlab.com/genus-project/).

The code & project structure has been inspired by two main resources.


* [Cobalt-Uoft](https://github.com/cobalt-uoft)
* [Flask-Example](https://github.com/MichaelDiBernardo/ddd-flask-example/)


The repo structure is as follows:
```
documentation - Entrypoint for the application, this is where you should go for more information about the project as a whole.

willow - The REST API that powers this applicaiton, following the UofT tree naming structure.

datasets - Datasets that are periodically updated by web scrapers.

scrapers - Web scrapers that aggregate data from 3 sources; coursefinder, course-evals, & RMP.
```
